# README #

El sistema esta conectado al servicio postal mexicano, cada que se consulte un cp, seran persistidos los datos y se guardaran en nuetra base de datos local, esto pensado para una v2 , donde se realizaria la consulta en la db si es que llegara a fallar la pagina oficial de servicio postal mexicano.Estaba pensado en que registrara los datos si no exisitian y mostrarlos desde la pagina ofical, y si ya existian los datos entonces mostrarlos desde la db, esto para que no dependiera directo de la pagina.



Este sistema funciona con una base de datos en MySQL local con los siguientes parametros:
password: zip_code
username: root
url: jdbc:mysql://localhost:3306/codigos_postales?useSSL=false&serverTimezone=America/Mexico_City&useLegacyDatetimeCode=false
nombre de la bs: codigos_postales




Tambien integracion de swangger para mejor integracion y control:
http://localhost:8080/swagger-ui.html



En el ambiente productivo opte por usar Heroku para integrar tambien la db , por el tiempo limitado:
credenciales de la db en productivo:
Schemal: heroku_e4b0c2eaab1af66
UserName: be2c40a8f24f57
Password: e079d152
HostName: us-cdbr-east-03.cleardb.com



Direccion local:
http://localhost:8080/zip-codes/{cp}
Direccion en despliegue:  
https://tu-codigo-postal-mx.herokuapp.com/zip-codes/{cp}



Ejemplo de consumo:
https://tu-codigo-postal-mx.herokuapp.com/zip-codes/50780
Respuesta:
[
    {
        "settlements": {
            "name": "San Ildefonso",
            "settlement_type": "Pueblo"
        },
        "city": " ",
        "municipality": "Ixtlahuaca",
        "official_key": "50741",
        "federal_entity": "México",
        "zip_code": "50780"
    },
    {
        "settlements": {
            "name": "San Lorenzo Toxico",
            "settlement_type": "Pueblo"
        },
        "city": " ",
        "municipality": "Ixtlahuaca",
        "official_key": "50741",
        "federal_entity": "México",
        "zip_code": "50780"
    },
    {
        "settlements": {
            "name": "San Lorenzo Toxico Manzana Sexta",
            "settlement_type": "Ejido"
        },
        "city": " ",
        "municipality": "Ixtlahuaca",
        "official_key": "50741",
        "federal_entity": "México",
        "zip_code": "50780"
    },
    {
        "settlements": {
            "name": "San Lorenzo Toxico Manzana Séptima",
            "settlement_type": "Ejido"
        },
        "city": " ",
        "municipality": "Ixtlahuaca",
        "official_key": "50741",
        "federal_entity": "México",
        "zip_code": "50780"
    },
    {
        "settlements": {
            "name": "San Lorenzo Toxico Manzana Octava",
            "settlement_type": "Ejido"
        },
        "city": " ",
        "municipality": "Ixtlahuaca",
        "official_key": "50741",
        "federal_entity": "México",
        "zip_code": "50780"
    },
    {
        "settlements": {
            "name": "San Francisco Ixtlahuaca",
            "settlement_type": "Pueblo"
        },
        "city": " ",
        "municipality": "Ixtlahuaca",
        "official_key": "50741",
        "federal_entity": "México",
        "zip_code": "50780"
    },
    {
        "settlements": {
            "name": "San Francisco",
            "settlement_type": "Ranchería"
        },
        "city": " ",
        "municipality": "Ixtlahuaca",
        "official_key": "50741",
        "federal_entity": "México",
        "zip_code": "50780"
    }
]



Las vesiones para el entorno de desarrollo son:
Apache Maven 3.6.3
git version 2.25.1
java version "1.8.0_281"
Spring tools suite 4
mysql version 8.0.23
Ubuntu 20.04.2



La ruta para poder clonar el proyecto desde git:
git clone https://jesusnava@bitbucket.org/jesusnava/apicodigopostal.git







